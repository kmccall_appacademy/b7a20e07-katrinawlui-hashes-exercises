# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_lengths_hash = Hash.new

  str.split.each { |word| word_lengths_hash[word] = word.length }

  word_lengths_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |k, v| v }.last.first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |k, v| older[k] = v }

  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_frequencies = Hash.new(0)

  word.chars { |letter| letter_frequencies[letter] += 1 }

  letter_frequencies
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  array_elements = Hash.new(0)

  arr.each { |el| array_elements[el] += 1 }

  array_elements.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  even_odd_count = {even: 0, odd: 0}

  numbers.each do |num|
    num.even? ? even_odd_count[:even] += 1 : even_odd_count[:odd] += 1
  end

  even_odd_count
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowel_count = {"a"=>0, "e"=>0, "i"=>0, "o"=>0, "u"=>0}

  string.chars do |letter|
    vowel_count[letter] += 1 if vowel_count.key?(letter)
  end

  largest_count = vowel_count.values.max
  frequent_vowels = vowel_count.select { |k, v| v == largest_count }.keys

  frequent_vowels.min
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  late_birthdays = students.select { |k, v| v >= 7 }

  late_birthdays.keys.combination(2)
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  specimen_population = Hash.new(0)

  specimens.each { |specimen| specimen_population[specimen] += 1 }

  number_of_species = specimen_population.length
  smallest_population_size = specimen_population.values.min
  largest_population_size = specimen_population.values.max

  number_of_species**2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  original_letters = character_count(normal_sign)
  vandalize_letters = character_count(vandalized_sign)

  vandalize_letters.all? do |letter, count|
    count <= original_letters[letter]
  end
end

def character_count(str)
  char_count = Hash.new(0)

  str.downcase.gsub(/[[:punct:]]/, "").chars do |char|
    char_count[char] += 1
  end

  char_count
end
